
"""
sudo apt-get install python3-tk
virtualenv --python python3.4 --system-site-packages env
source env/bin/activate
pip install matplotlib
python main.py
"""


import matplotlib.pyplot as plt
import numpy as np
import time
import random

fig = plt.figure()
ax = fig.add_subplot(111)

# some X and Y data
max_money = 1000
x = np.arange(max_money)
y = np.random.randn(max_money)

li, = ax.plot(x, y)

# draw and show it
ax.axis([-10.0, max_money, 0.0, 50.0])
# ax.relim()
# ax.autoscale_view(True,True,True)
fig.canvas.draw()
plt.show(block=False)

people_count = 1000
initial_money = 100
world_money = initial_money * people_count
changes_per_frame = 100000
people = [initial_money for x in range(people_count)]

print("press CTRL-C to stop the program")

# loop to update the data
while True:
    try:
        # for i in range(max_money):
        #     y[i] = people.count(i)

        y = np.zeros(max_money)
        for money in people:
            if money < y.size:
                y[money] += 1

        for i in range(changes_per_frame):
            person_1 = random.randrange(people_count)
            person_2 = random.randrange(people_count)
            if people[person_1] > 0:
                people[person_1] -= 1
                people[person_2] += 1

        # ax.relim()
        # set the new data
        li.set_ydata(y)

        fig.canvas.draw()

        accumulated_people = 0
        accumulated_money = 0
        i = 0
        population_percentage = 0.8

        while accumulated_people < population_percentage * people_count:
            accumulated_people += y[i]
            accumulated_money += y[i]*i
            i = i + 1
        population_percentage_person = i

        it_percentage_people = 0.0
        it_percentage_money = 0.0
        i = 0
        while it_percentage_people + it_percentage_money < 1.0:
            it_percentage_people += y[i]/people_count
            it_percentage_money += y[i]*i/world_money
            i = i + 1
        it_person = i

        it_people_median = 0
        i = 0
        while it_people_median < people_count//2:
            it_people_median += y[i]
            i += 1
        median = i

        it_people_average = 0
        i = 0
        average_money = world_money / people_count
        while i < average_money:
            it_people_average += y[i]
            i += 1

        fig.suptitle("\n\n\na random has {}\nrichest has {}\n"
                     "median has {} and {:.3}% people below\n"
                     "average has {} and {:.3}% people below\n"
                     "bottom {:.3}% of people (have {} or less) has {:.3}% of money\n"
                     "bottom {:.3}% of people (have {} or less) has {:.3}% of money".format(
            people[0], max(people), median, it_people_median/people_count *100,
            average_money, it_people_average/people_count *100,
            population_percentage*100, population_percentage_person, accumulated_money/world_money * 100,
            it_percentage_people*100, it_person, it_percentage_money*100))

        time.sleep(0.01)
    except KeyboardInterrupt:
        break

