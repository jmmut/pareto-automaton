# Pareto distribution

This is a one-afternoon project, so the code is ugly, it's terribly slow, with a plot library that is probably not designed to do this, but I wanted to see if I could come up with some interesting plots and percentages.

Recently I came up with a [claim from Jordan Peterson](https://www.youtube.com/watch?v=w84uRYq0Uc8&t=21m16s), where he said that if you play a game where 1000 people receive 100 dollars, and they trade randomly (flipping a coin) 1 dollar at a time, at the end one person will accumulate all the money.

Of course this is a simple example to explain the pareto distribution, where resources are concentrated in a small fraction of any population, but I was curious if such a simple scenario would actually yield numbers that resemble pareto distribution. The idea is similar to how you can state equations that must hold for every moment (but the equations are often not enough to build a system that shows those properties), or you can define behaviour (such as cellular automata) and then derive which equations describe that behaviour. The pareto distribution are the equations and this program is the "cellular automaton" that only defines simple behaviour.

This distribution is also called the 80-20 rule.

In the first prototype I used the rule that if any person reaches 0 money, then it stops trading. This yields the result that Peterson was talking about, because it locks the people out of the game, and with random walks, eventually everyone will wander around having 0, getting locked out, and leaving less and less people in the game. With enough time, I guess two people with half the money each will remain, and with enough time, a random walk will lead one of them to 0, leaving only one with all the money, who can not trade anymore. So what Peterson said is true, but only because of a technicality, and it didn't show pareto-like numbers.

As I wanted to make the game more interesting, so I set the rule that having 0 money, you can still trade. If you win the trade, you get the dollar, and if you lose, nothing happens. With this new rule, this is an example run:

![the image couldn't load](average.png)

The horizontal axis is wealth (rich people on the right, poor people on the left) and the vertical axis is frequency (if the line is near the bottom means that very few people have that wealth, and a higher line means that a lot of people have that wealth).

So it seems that the richest person quickly goes up to 600 dollars and continues growing. The median is not 100 as you might expect, but it is 74. To keep the average wealth (which is how everything starts) you have to be over 61% of the people. 80% of the population holds 49% percent of the money. The point where percentages swap is at 68% of the population holding 32% of the money. Percentages usually go up and down 2 percentual points and the dollars also vary around 3 dollars up and down.

Actually wikipedia mentions that:

> Pareto's data on British income taxes in his Cours d'‚conomie politique indicates that about 30% of the population had about 70% of the income.

Of course, it would be interesting to run more experiments with more people during more time, but the afternoon has ended, the program is too slow and I want to sleep.

